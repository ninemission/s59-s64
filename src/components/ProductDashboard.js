import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form, Badge, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ProductPanel() {
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [isNewProduct, setIsNewProduct] = useState(true);
  const [productId, setProductId] = useState('');
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [productImage, setProductImage] = useState('');
  const [productActive, setProductActive] = useState(true);
  const [searchText, setSearchText] = useState('');
  const [productCategory, setProductCategory] = useState('');

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(response => response.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.error('Error fetching products:', error);
      });
  };

  const handleModalShow = () => {
    setShowModal(true);
    setIsNewProduct(true);
    resetForm();
  };

  const handleModalClose = () => {
    setShowModal(false);
    setIsNewProduct(true);
    resetForm();
  };

  const resetForm = () => {
    setProductId('');
    setProductName('');
    setProductDescription('');
    setProductPrice('');
    setProductActive(true);
    setProductImage('')
  };

  const handleCreateProduct = () => {
      const categoriesArray = typeof productCategory === 'string'
    ? productCategory.includes(',')
      ? productCategory.split(',').map((category) => category.trim())
      : [productCategory.trim()]
    : [];
    const productData = {
      name: productName,
      description: productDescription,
      price: productPrice,
      isActive: productActive,
      image: productImage,
      category: categoriesArray,
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(productData)
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Create Successful',
            icon: 'success',
            text: 'Product creation success'
          });
          fetchProducts();
          handleModalClose();
        } else {
          Swal.fire({
            title: 'Create Unsuccessful',
            icon: 'error',
            text: 'Please try again'
          });
        }
      })
      .catch(error => {
        console.error('Error creating product:', error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while creating the product'
        });
      });
  };

  const handleUpdateProduct = () => {
      const categoriesArray = typeof productCategory === 'string'
    ? productCategory.includes(',')
      ? productCategory.split(',').map((category) => category.trim())
      : [productCategory.trim()]
    : [];
    const productData = {
      name: productName,
      description: productDescription,
      price: productPrice,
      isActive: productActive,
      image: productImage,
      category: categoriesArray,
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(productData)
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Update Successful',
            icon: 'success',
            text: 'Product update success'
          });
          fetchProducts();
          handleModalClose();
        } else {
          Swal.fire({
            title: 'Update Unsuccessful',
            icon: 'error',
            text: 'Please try again'
          });
        }
      })
      .catch(error => {
        console.error('Error updating product:', error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while updating the product'
        });
      });
  };

  const handleStatusChange = (productId, isActive) => {
    const endpoint = isActive ? `/products/${productId}/archive` : `/products/${productId}/activate`;

    fetch(`${process.env.REACT_APP_API_URL}${endpoint}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Status Change Successful',
            icon: 'success',
            text: 'Product status change success'
          });
          fetchProducts();
        } else {
          Swal.fire({
            title: 'Status Change Unsuccessful',
            icon: 'error',
            text: 'Please try again'
          });
        }
      })
      .catch(error => {
        console.error('Error changing product status:', error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while changing the product status'
        });
      });
  };

  const handleEditProduct = product => {
    setShowModal(true);
    setIsNewProduct(false);
    setProductId(product._id);
    setProductName(product.name);
    setProductDescription(product.description);
    setProductPrice(product.price);
    setProductImage(product.image);
    setProductCategory(product.category);
  };

  const handleSearch = event => {
    setSearchText(event.target.value);
  };

  const filteredProducts = products.filter(product =>
    product.name.toLowerCase().includes(searchText.toLowerCase())
  );

  return (
    <Container>
      <h1 className="text-center my-3">Product Dashboard</h1>
      <div className="text-center">
        <Button className="my-3 bg-primary" variant="primary" onClick={handleModalShow}>
          Create New Product
        </Button>
      </div>
      <Form.Group className="center my-3" controlId="searchForm">
        <Form.Control type="text" placeholder="Search by Name" value={searchText} onChange={handleSearch} />
      </Form.Group>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredProducts.map(product => (
            <tr key={product._id}>
              <td>
                <div className="d-flex align-items-center">
                  <img
                    src={product.image}
                    alt={product.name}
                    style={{ width: '45px', height: '45px' }}
                    className="rounded-circle"
                  />
                  <div className="ms-3">
                    <p className="fw-bold mb-1">{product.name}</p>
                    <p className="text-muted mb-0">{product._id}</p>
                  </div>
                </div>
              </td>
              <td>{product.description}</td>
              <td>{product.price}</td>
              <td>
                {product.isActive ? (
                  <Badge bg="success" style={{ fontSize: '14px' }}>
                    Active
                  </Badge>
                ) : (
                  <Badge bg="danger" style={{ fontSize: '14px' }}>
                    Inactive
                  </Badge>
                )}
              </td>
              <td>
                <Button variant="outline-primary" className="me-2" onClick={() => handleEditProduct(product)}>
                  Edit
                </Button>
                <Button
                  variant={product.isActive ? 'outline-danger' : 'outline-success'}
                  onClick={() => handleStatusChange(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Reactivate'}
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>{isNewProduct ? 'Create New Product' : 'Edit Product'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control type="text" value={productName} onChange={e => setProductName(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} value={productDescription} onChange={e => setProductDescription(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" value={productPrice} onChange={e => setProductPrice(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productCategory">
              <Form.Label>Category</Form.Label>
              <Form.Control type="text" value={productCategory} onChange={e => setProductCategory(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productImage">
              <Form.Label>Product Image</Form.Label>
              <Form.Control type="text" value={productImage} onChange={e => setProductImage(e.target.value)} />
              <img src={productImage} alt="Product" style={{ width: '100%', marginTop: '10px' }} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModalClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={isNewProduct ? handleCreateProduct : handleUpdateProduct}>
            {isNewProduct ? 'Create' : 'Update'}
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}