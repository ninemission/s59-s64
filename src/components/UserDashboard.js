import { useEffect, useState } from 'react';
import { Table, Button, Badge, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function UserDashboard() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => setUsers(data))
      .catch((error) => console.error('Error fetching users:', error));
  };

  return (
    <Container>
      <h1 className="text-center my-3">User Dashboard</h1>
      <Table bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile Number</th>
            <th>Admin</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user._id}>
              <td>
                <div className="d-flex align-items-center">
                  <img
                    src={user.image}
                    alt={`${user.firstName} ${user.lastName}`}
                    style={{ width: '45px', height: '45px' }}
                    className="rounded-circle"
                  />
                  <div className="ms-3">
                    <p className="fw-bold mb-1">{`${user.firstName} ${user.lastName}`}</p>
                    <p className="text-muted mb-0">{user._id}</p>
                  </div>
                </div>
              </td>
              <td>{user.email}</td>
              <td>{user.mobileNo}</td>
              <td>
                {user.isAdmin ? (
                  <Badge bg="success" style={{ fontSize: '14px' }}>
                    Admin
                  </Badge>
                ) : (
                  <Badge bg="warning" text="dark" style={{ fontSize: '14px' }}>
                    User
                  </Badge>
                )}
              </td>
              <td>
                {/* Update the link to point to the single user details page */}
                <Button as={Link} to={`/users/${user._id}`} variant="primary" size="sm">
                  View Profile & Order History
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
}