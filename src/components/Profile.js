import { Row, Col, Button, Table, Container } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import Swal2 from 'sweetalert2';
import { useNavigate, useParams } from 'react-router-dom';

export default function Profile() {
  const { userId } = useParams();
  const [userDetails, setUserDetails] = useState(null);
  const [orderHistory, setOrderHistory] = useState([]);
  const [showOrderHistory, setShowOrderHistory] = useState(false);
  const token = localStorage.getItem('token');
  const navigate = useNavigate();

  useEffect(() => {
    fetchUserDetails();
  }, []);

  const fetchUserDetails = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setUserDetails(data);
      })
      .catch(error => {
        console.error('Error fetching user details:', error);
      });
  };

  const fetchOrderHistory = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/orderDetails`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {
        setOrderHistory(data);
        setShowOrderHistory(true);
      })
      .catch(error => {
        console.error('Error fetching order history:', error);
      });
  };

  const handleCloseOrderHistory = () => {
    setShowOrderHistory(false);
  };

  if (!token) {
    navigate('/*');
    return null;
  }

  return (
    <Container>
      <Row>
        <Col className="ProfileDetails mt-3" md={6}>
          {userDetails ? (
            <>
              <h2>{userDetails.firstName} {userDetails.lastName}</h2>
              <p>Email: {userDetails.email}</p>
              <p>Mobile No: {userDetails.mobileNo}</p>
              <p>Admin: {userDetails.isAdmin ? 'Yes' : 'No'}</p>
              {!userDetails.isAdmin && ( // Use userDetails.isAdmin to conditionally render the button
                <Button variant="primary" onClick={fetchOrderHistory}>
                  Show Order History
                </Button>
              )}
            </>
          ) : (
            navigate('/*')
          )}
        </Col>
      </Row>

      {showOrderHistory && orderHistory.length > 0 && (
        <Row>
          <Col className="ProfileOrderHistory mt-5" md={12}>
            <h3>Order History</h3>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Order ID</th>
                  <th>Order Date</th>
                  <th>Item</th>
                  <th>Quantity</th>
                  <th>Total Price</th>
                </tr>
              </thead>
              <tbody>
                {orderHistory.map(order => (
                  <tr key={order._id}>
                    <td>{order._id}</td>
                    <td>{new Date(order.purchasedOn).toLocaleDateString()}</td>
                    <td>
                      {order.products.map(item => (
                        <div key={item.name}>{item.name}</div>
                      ))}
                    </td>
                    <td>
                      {order.products.map(item => (
                        <div key={item.productId}>{item.quantity}</div>
                      ))}
                    </td>
                    <td>{order.totalAmount}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <Button variant="secondary" onClick={handleCloseOrderHistory}>
              Close Order History
            </Button>
          </Col>
        </Row>
      )}
    </Container>
  );
}