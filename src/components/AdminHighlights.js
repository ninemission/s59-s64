import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminHighlights() {
  return (
    <Row>
      <Col className="p-5">
        <h2 className="mb-4 text-center">Admin Dashboard</h2>
        <Row>
          <Col md={6} sm={6}>
            <Card as={Link} to="/admin/userpanel" className="mb-3">
              <Card.Img
                variant="top"
                src="https://images.unsplash.com/photo-1543269865-cbf427effbad?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80"
                alt="User Dashboard"
                className="card-img"
              />
              <Card.Body>
                <Card.Title className = "text-center">User Dashboard</Card.Title>
                <Card.Text>
                <em>
                  View and manage user accounts, access user data, and perform administrative tasks related to users.
                </em>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col md={6} sm={6}>
            <Card as={Link} to="/admin/productpanel" className="mb-3">
              <Card.Img
                variant="top"
                src="https://images.pexels.com/photos/6696932/pexels-photo-6696932.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                alt="Product Dashboard"
                className="card-img"
              />
              <Card.Body>
                <Card.Title className = "text-center">Product Dashboard</Card.Title>
                <Card.Text>
                <em>
                  Manage product listings, create new products, edit existing products, and activate or deactivate products for display.
                </em>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}