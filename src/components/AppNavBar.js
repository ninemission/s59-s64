import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

import { useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          Poppy's
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {user.id ? (
              user.isAdmin ? (
                <>
                  <Nav.Link as={NavLink} to="/admin">
                    Home
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/products">
                    Products
                  </Nav.Link>
                  <NavDropdown title="My Workspace" id="basic-nav-dropdown">
                    <NavDropdown.Item as={NavLink} to="/admin/productpanel">
                      Product Dashboard
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/admin/userpanel">
                      User Dashboard
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={NavLink} to="/myprofile">
                      View Profile
                    </NavDropdown.Item>
                  </NavDropdown>
                  <Nav.Link as={NavLink} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/">
                    Home
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/products">
                    Products
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/myprofile">
                    Profile
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              )
            ) : (
              <>
                <Nav.Link as={NavLink} to="/">
                  Home
                </Nav.Link>
                <Nav.Link as={NavLink} to="/products">
                  Products
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  Register
                </Nav.Link>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}