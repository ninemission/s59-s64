import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Highlights() {
  return (
    <Row>
      <Col className="p-5">
        <h2 className="text-center mb-4 highlights-heading">Discover Our Specialties</h2>
        <Row>
          <Col md={4} sm={6}>
            <Card as={Link} to="/products?category=Snacks" className="mb-3 highlight-card">
              <Card.Img
                variant="top"
                src="https://images.pexels.com/photos/9068955/pexels-photo-9068955.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                alt="Highlight 1"
                className="card-img"
              />
              <Card.Body>
                <Card.Title className="text-center highlight-title" style={{ textDecoration: 'none' }}>
                  Snacks
                </Card.Title>
                <Card.Text className="text-center highlight-description" style={{ textDecoration: 'none' }}>
                  <em>
                    Indulge in a delightful assortment of snacks that will satisfy your cravings.
                    Our collection offers a blend of savory and sweet treats, perfect for any occasion.
                  </em>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col md={4} sm={6}>
            <Card as={Link} to="/products?category=Beverages" className="mb-3 highlight-card">
              <Card.Img
                variant="top"
                src="https://images.unsplash.com/photo-1572771006151-33d4c6ed2353?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=764&q=80"
                alt="Highlight 2"
                className="card-img"
              />
              <Card.Body>
                <Card.Title className="text-center highlight-title" style={{ textDecoration: 'none' }}>
                  Beverages
                </Card.Title>
                <Card.Text className="text-center highlight-description" style={{ textDecoration: 'none' }}>
                  <em>
                    Quench your thirst with our selection of refreshing beverages.
                    From smoothies and shakes to aromatic coffees and tantalizing teas,
                    we offer a variety of options to suit your taste.
                  </em>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col md={4} sm={6}>
            <Card as={Link} to="/products?category=Sweets" className="mb-3 highlight-card">
              <Card.Img
                variant="top"
                src="https://images.pexels.com/photos/5859262/pexels-photo-5859262.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                alt="Highlight 3"
                className="card-img"
              />
              <Card.Body>
                <Card.Title className="text-center highlight-title" style={{ textDecoration: 'none' }}>
                  Sweets
                </Card.Title>
                <Card.Text className="text-center highlight-description" style={{ textDecoration: 'none' }}>
                  <em>
                    Experience pure bliss with our heavenly selection of sweets.
                    From decadent chocolates to mouthwatering pastries,
                    each bite is a journey of delight and indulgence.
                  </em>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}