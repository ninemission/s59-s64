import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function LandingPage() {
  const backgroundImageUrl = 'https://images.pexels.com/photos/9425647/pexels-photo-9425647.png';

  const landingPageStyle = {
    backgroundImage: `url(${backgroundImageUrl})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: 'white',
    textShadow: '2px 2px 4px rgba(0, 0, 0, 0.3)',
    padding: '3rem',
    filter: 'brightness(110%)', // Adjust the brightness level (110% makes it slightly brighter)
  };

  const headingStyle = {
    fontSize: '4rem',
    marginBottom: '2rem',
    marginTop: '2rem',
  };

  const descriptionStyle = {
    fontSize: '1.5rem',
    marginBottom: '2rem',
  };

  const subDescriptionStyle = {
    fontSize: '1.2rem',
    marginBottom: '2rem',
  };

  const buttonStyle = {
    fontSize: '1.5rem',
    padding: '1rem 2rem',
  };

  return (
    <div className="landing-page" style={landingPageStyle}>
      <Container>
        <h1 style={headingStyle}>Welcome to Poppy's</h1>
        <p style={descriptionStyle}>
          We offer a delightful selection of Snacks, Sweets, and Beverages.
          <br />
          Find your favorites and satisfy your cravings with us.
        </p>
        <hr className="my-4" />
        <p style={subDescriptionStyle}>
          It's snack time! Browse our products and discover a world of delicious treats.
        </p>
        <p className="lead">
          <Button as={Link} to="/products" variant="light" style={buttonStyle}>
            Explore Now
          </Button>
        </p>
      </Container>
    </div>
  );
}