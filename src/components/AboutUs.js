import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

function AboutUs() {
  return (
    <div className="about-us bg-light" style={{ height: '75vh', background: '#87ceeb' }}>
      <Container fluid style={{ height: '100%' }}>
        <Row className="align-items-center" style={{ height: '100%' }}>
          {/* Text on the left, Image on the right for medium and large devices */}
          <Col md={6} className="px-5">
            <div className="about-us-text">
              <h2>Meet Poppy</h2>
              <p>
                His name is Philip, but his friends call him Pipoy, which inspired the name Poppy.
              </p>
              <p>
                In 1998, Poppy started his journey in the snack distribution business,
                working with various brands and building strong connections in the industry.
              </p>
              <p>
                With dedication and hard work, Poppy has been delivering delicious snacks and
                treats to customers all around the world, and his passion for quality and
                excellence keeps him going strong.
              </p>
              <p>
                Poppy's offers a delightful selection of snacks, sweets, and beverages.
                From classic favorites to unique delights, we have something for every craving.
                Our commitment to quality and customer satisfaction is what sets us apart,
                and we take pride in delivering the best snacks to your doorstep.
                Join us in this delicious journey and discover a world of delightful treats.
              </p>
              {/* Add more details about Poppy or your business here */}
            </div>
          </Col>
          <Col md={6} className="px-5">
            {/* Image on top, Text on bottom for small devices */}
            <div className="about-us-image">
              <img
                src="https://img.freepik.com/premium-vector/delivery-logistics-warehouse-parcels-loading-unloading-by-workers-shipping-truck_316839-583.jpg?w=1800"
                alt="Poppy"
                className="img-fluid"
                style={{ height: '100%', width: '100%', objectFit: 'cover' }}
              />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default AboutUs;