import { Row, Col, Button, Form, Modal, Container, Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import Swal2 from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductView() {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [productImage, setProductImage] = useState('');
  const [productCategory, setProductCategory] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [showModal, setShowModal] = useState(false);

  const { id } = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext); // Accessing the UserContext to get the user object

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((response) => response.json())
      .then((data) => {
        setName(data.name);
        setPrice(data.price);
        setDescription(data.description);
        setProductImage(data.image);
        setProductCategory(data.category);
      });
  }, [id]);

  const handleIncrement = () => {
    setQuantity((prevQuantity) => prevQuantity + 1);
  };

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity((prevQuantity) => prevQuantity - 1);
    }
  };

  const handleOrder = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleConfirmOrder = () => {
    const token = localStorage.getItem('token');

    if (!user) {
      Swal2.fire({
        title: 'Order failed',
        icon: 'error',
        text: 'You are not logged in. Please log in to place the order.',
        confirmButtonText: 'Ok',
      }).then(() => {
        navigate('/login');
      });
      return;
    }

    if (user.isAdmin) {
      Swal2.fire({
        title: 'Order Prohibited',
        icon: 'warning',
        text: 'You are an admin and cannot place orders.',
        confirmButtonText: 'Ok',
      });
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        productId: id,
        quantity: quantity,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
        if (data == true) {
          // successful order
          Swal2.fire({
            title: 'Order successful',
            icon: 'success',
            text: 'Your order has been placed successfully!',
          });
        } else {
          Swal2.fire({
            title: 'Order unsuccessful',
            icon: 'error',
            text: 'Please try again!',
            confirmButtonText: 'Ok',
          });
        }
        setShowModal(false);
      });
  }


  const totalPrice = price * quantity;

  return (
    <Container>
    <Row className="justify-content-center mt-5">
      <Col md={4}>
        <img src={productImage} alt={name} style={{ width: '100%', height: 'auto' }} />
      </Col>
      <Col md={8}>
        <h3>{name}</h3>
        <p>{description}</p>
        <p className="text-danger">Php {price}</p>
        <p>Category: {`${productCategory}`}</p>

        <div className="d-flex align-items-center justify-content-start">
          <Button variant="light" onClick={handleDecrement} style={{ marginRight: '10px' }}>
            -
          </Button>
          <Form.Control type="text" value={quantity} readOnly style={{ width: '50px', textAlign: 'center' }} />
          <Button variant="light" onClick={handleIncrement} style={{ marginLeft: '10px' }}>
            +
          </Button>
        </div>

        <p className="mt-3">Total Price: {totalPrice}</p>
        <Button variant="primary" onClick={handleOrder}>
          Order Now
        </Button>
      </Col>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Order Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Product Name: {name}</p>
          <p>Quantity: {quantity}</p>
          <p>Total Price: {totalPrice}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="primary" onClick={handleConfirmOrder}>
            Confirm Order
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
    </Container>
  );
}