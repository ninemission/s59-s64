import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, image, category } = productProp;

  return (
    <div className="product-card-container">
      <Card className="shadow-sm border rounded mb-3 product-card d-flex flex-column justify-content-between align-items-center" >
        <Card.Img
          className="productImage mt-3"
          variant="top"
          src={image}
        />
        <Card.Body className="d-flex flex-column align-items-center">
          <div className="text-center">
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <div className="text-muted small mb-3">
              <h4 className="text-danger mb-0">Php {price}</h4>
              {Array.isArray(category) ? (
                category.map((cat, index) => (
                  <span key={index}>
                    {cat}
                    {index !== category.length - 1 && <span className="text-primary"> • </span>}
                  </span>
                ))
              ) : (
                <span>{category}</span>
              )}
            </div>
          </div>
          <Button
            as={Link}
            to={`/products/${_id}`}
            variant="primary"
            size="sm"
            style={{ padding: '5px 10px' }}
          >
            Details
          </Button>
        </Card.Body>
      </Card>
    </div>
  );
}

ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    category: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]).isRequired,
  }),
};
export function TrialFunction(){

}
