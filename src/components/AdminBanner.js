import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminBanner() {
  return (
    <div
      className="admin-landing-page"
      style={{
        backgroundImage: "url('https://images.pexels.com/photos/927022/pexels-photo-927022.jpeg?auto=compress&cs=tinysrgb&w=5760&h=3840')",
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center calc(50% + 140px)',
        minHeight: '75vh',
        position: 'relative',
      }}
    >
      <div
        className="overlay"
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }}
      />
      <Container>
        <Row>
          <Col md={6}>
            <h1 className="display-md-3" style={{ color: '#fff', zIndex: 1, position: 'relative' }}>
              Welcome Admin
            </h1>
            <p className="lead" style={{ color: '#fff', marginBottom: '2rem', zIndex: 1, position: 'relative' }}>
              Manage Your Dashboard with Ease
            </p>
            <p style={{ color: '#fff', marginBottom: '2rem', zIndex: 1, position: 'relative' }}>
              Welcome to your admin dashboard! This is your central hub for managing all aspects of your website. From here, you can easily add, edit, and delete products, manage user accounts, and keep track of your website's performance. With a user-friendly interface and powerful tools, you have everything you need to take your business to new heights.
            </p>
            <hr className="my-4" />
          </Col>
        </Row>
      </Container>
    </div>
  );
}