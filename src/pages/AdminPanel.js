import { Fragment } from 'react';
import AdminBanner from '../components/AdminBanner';
import AdminHighlights from '../components/AdminHighlights';
import { Container } from 'react-bootstrap';


export default function AdminPanel() {
  return (
  	<Fragment>	
  		<AdminBanner />
  		<Container>
  		<AdminHighlights />
  		</Container>
  	</Fragment>
  );
}