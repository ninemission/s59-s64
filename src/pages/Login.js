import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container, Card} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';


export default function Login() {

    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    // Allows us to consume the UserContext object and it's properties to use for user validation
    const { user, setUser} = useContext(UserContext);

    // const [user, setUser] = useState(localStorage.getItem('email'));

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data === false){
                Swal2.fire({
                    title: "Login unsuccessful",
                    icon: 'error',
                    text: 'Check your login credentials and try again'
                })
            } else {
                localStorage.setItem('token', data.access)

                retrieveUserDetails(data.access);

                // alert('Login successful!')

                Swal2.fire({
                    title: 'Login successful',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'

                })
            }
        })
    }

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(response => response.json())
    .then(data => {
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        });
    if (data.isAdmin){
        navigate('/admin')
    } else {
        navigate('/')
    }
})
}


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(false);
        }else{
            setIsActive(true);
        }

    }, [email, password]);

    return (
          user.id === null || user.id === undefined
        ?
       <div className="login-page">
      <style>
        {`
          body {
            background-color: rgb(238,174,202);
            background: radial-gradient(circle, rgba(238,174,202,1) 0%, rgba(7,16,35,1) 100%);
          }
        `}
      </style>
                <Container className="my-5">
      <Card>
        <Row className="g-0">
          <Col md={6}>
           <div style={{ height: '100%' }}>
            <Card.Img
              src="https://media.istockphoto.com/id/692533484/vector/people-eat-3.jpg?s=612x612&w=0&k=20&c=6uBN7YC4zggfZnonOFXyNVPweWOXRG2-CXF1UpQ6c0k="
              alt="login form"
              className="rounded-start w-100 img-fluid"
              style={{ height: '100%' }}
            />
            </div>
          </Col>
          <Col md={6}>
            <Card.Body className="d-flex flex-column">
              <div className="d-flex flex-row mt-2">
                <img
                      src="/logo.png" // Update this path to the relative path of your logo image
                      alt="Logo"
                      width="60" // Set the desired width for the image
                      height="60" // Set the desired height for the image
                      className="bi bi-cube-fill me-3"
                      style={{ objectFit: 'cover' }} // Maintain the aspect ratio without stretching
                    />
                <span className="h1 fw-bold mb-0">Poppy's</span>
              </div>
              <h5 className="fw-normal my-4 pb-3" style={{ letterSpacing: '1px' }}>
                Sign into your account
              </h5>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group className="mb-4" controlId="formEmail">
                  <Form.Label>Email address</Form.Label>
                  <div className="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"  viewBox="0 0 16 16" className = "me-3">
                    <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                    </svg>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                  </div>
                </Form.Group>
                <Form.Group className="mb-4" controlId="formPassword">
                  <Form.Label>Password</Form.Label>
                  <div className="d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"  viewBox="0 0 16 16" className="me-3">
                    <path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                    </svg>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                  </div>
                </Form.Group>
                <Button className="mb-4 px-5" variant="dark" type="submit" disabled={isActive}>
                  Login
                </Button>
              </Form>
              <p className="mb-5 pb-lg-2" style={{ color: '#393f81' }}>
                Don't have an account? <a href="#!" style={{ color: '#393f81' }}>Register here</a>
              </p>
              <div className="d-flex flex-row justify-content-start">

              </div>
            </Card.Body>
          </Col>
        </Row>
      </Card>
    </Container>
    </div>           
        :
            <Navigate to = '*' />
    )
}
