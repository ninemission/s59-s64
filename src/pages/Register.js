import {Button, Form, Row, Col, Container, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2';

export default function Register(){

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	// State that will declare whether mobile number is 11 char
	const [isNumPassed, setNumIsPassed] = useState(true);

	const [isPassed, setIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	//we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 

	const { user, setUser} = useContext(UserContext);

	const navigate = useNavigate();
 

	//This useEffect will disable or enable our sign up button
	useEffect(()=> {
		//We are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '' && password1 === password2 && mobileNumber.length <=11){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2]);


	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();

	// Checking if email exist (fetch checkEmail from backend)
	fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email:email
		})
	})
	.then(response => response.json())
	.then(data => {
		if (data === true){
			Swal2.fire({
				title: 'Duplicate email found!',
				icon: 'error',
				text: 'Please provide different email'
			})
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNumber
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data === false){
					Swal2.fire({
						title: 'Registration Unsuccessful!',
						icon: 'error',
						text: 'Please try again'
					})
				} else {
					Swal2.fire({
						title: 'Registration Successful!',
						icon: 'success',
						text: 'You are now registered to Zuitt'
					})
					navigate('/login');
				}

			})

		}
	})
	}


	// check if mobileNo is 11 characters
	useEffect(() => {
		if(mobileNumber.length > 11){
			setNumIsPassed(false)
		}else{
			setNumIsPassed(true)
		}
	}, [mobileNumber])

	//useEffect to validate whether the password1 is equal to password2
	useEffect(() => {
		if(password1 !== password2){
			setIsPasswordMatch(false);
		}else{
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return(
		user.id === null || user.id === undefined
		?
		<Container fluid>
		      <style>
        {`
          body {
            background-color: rgb(238,174,202);
            background: radial-gradient(circle, rgba(238,174,202,1) 0%, rgba(7,16,35,1) 100%);
            }
          }
        `}
      </style>
      <Container className = 'my-5'>
			<Card className="text-black m-5" style={{ borderRadius: '25px' }}>
				<Card.Body>
					<Row>
						<Col md="10" lg="6" className="order-2 order-lg-1 align-items-center">
							<p className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign up</p>
							<Form onSubmit ={event => registerUser(event)}>
								<Form.Group className="mb-4" controlId="formFirstName">
									<Form.Label className="mb-1">First Name</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16" className="me-3">
										<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
										<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
										</svg>
										<Form.Control type="text" placeholder="Your First Name" value={firstName} onChange={event => setFirstName(event.target.value)} />
									</div>
								</Form.Group>

								<Form.Group className="mb-4" controlId="formLastName">
									<Form.Label>Last Name</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16" className="me-3" size="lg">
										<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
										<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
										</svg>
										<Form.Control type="text" placeholder="Your Last Name" className="w-100" value={lastName} onChange={event => setLastName(event.target.value)} />
									</div>
								</Form.Group>

								<Form.Group className="mb-4" controlId="formBasicEmail">
									<Form.Label>Email Address</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16" className = "me-3">
										<path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
										</svg>
										<Form.Control type="email" placeholder="Your Email" value={email} onChange={event => setEmail(event.target.value)} />
									</div>
								</Form.Group>

								<Form.Group className="mb-4" controlId="formMobileNo">
									<Form.Label>Mobile Number</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16" className = "me-3">
										<path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
										</svg>
										<Form.Control type="tel" placeholder="Your Mobile Number" className="w-100" value={mobileNumber} onChange={event => setMobileNumber(event.target.value)} />
									</div>
									<Form.Text className="text-danger" hidden = {isNumPassed}>Mobile number should have 11 characters.</Form.Text>
								</Form.Group>

								<Form.Group className="mb-4" controlId="formBasicPassword1">
									<Form.Label>Password</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-key-fill" viewBox="0 0 16 16" className="me-3">
										<path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
										</svg>
										<Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} />
									</div>
								</Form.Group>

								<Form.Group className="mb-4" controlId="formBasicPassword2">
									<Form.Label>Confirm Password</Form.Label>
									<div className="d-flex align-items-center">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-key-fill" viewBox="0 0 16 16" className="me-3">
										<path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
										</svg>
										<Form.Control type="password" placeholder="Repeat your password" value={password2} onChange={event => setPassword2(event.target.value)} />
									</div>
									<Form.Text className="text-danger" hidden = {isPasswordMatch}>The passwords does not match!</Form.Text>
								</Form.Group>

								<div className = "text-center">
									<Button className="mb-4" size="lg" type ="submit" disabled={isDisabled}>
									Register
									</Button>
								</div>
							</Form>
						</Col>

						<Col md="10" lg="6" className="order-1 order-lg-2 d-flex align-items-center">
							<Card.Img src="https://media.istockphoto.com/id/922632930/vector/people-eat.jpg?s=612x612&w=0&k=20&c=qb1cI-Ay-OSb6ejTVdrF2_flBqHbWutO-8TKbSYNou8=" fluid />
						</Col>
					</Row>
				</Card.Body>
			</Card>
        </Container>
        </Container>
		:
		<Navigate to = '*' />
		)
}