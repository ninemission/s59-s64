import {Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound(){

	return (
		<Row>
			<Col className = 'col-4 mx-auto'>
				<h1 className = 'text-center text-primary'>Page Not Found</h1>
				<p className = 'text-center'> Go back to the <Link to = '/'>homepage</Link></p>
			</Col>
		</Row>
	)
}