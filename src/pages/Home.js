import { Fragment } from 'react';
import LandingPage from '../components/Banner';
import Highlights from '../components/Highlights';
import AboutUs from '../components/AboutUs';
import { Container } from 'react-bootstrap';

export default function Home(){
	
	return (
		<Fragment>	
			<LandingPage />
			
			<Highlights />
			
			<AboutUs />
		</Fragment>
	)
}
