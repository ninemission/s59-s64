import { Fragment, useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import { Row, Col, Container } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

export default function Products() {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const location = useLocation();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const categoryFromURL = params.get('category');
    setSelectedCategory(categoryFromURL);
  }, [location]);

  useEffect(() => {
    if (selectedCategory) {
      const filtered = products.filter((product) => {
        if (Array.isArray(product.category)) {
          return product.category.includes(selectedCategory);
        } else {
          return product.category === selectedCategory;
        }
      });
      setFilteredProducts(filtered);
    } else {
      setFilteredProducts(products);
    }
  }, [products, selectedCategory]);

  const handleCategoryFilter = (category) => {
    setSelectedCategory(category);
  };

  const getSubtitleByCategory = (category) => {
    switch (category) {
      case 'Snacks':
        return 'Tasty and crunchy snacks to satisfy your cravings';
      case 'Sweets':
        return 'Delightful sweets to indulge your sweet tooth';
      case 'Beverages':
        return 'Refreshing and energizing beverages to quench your thirst';
      default:
        return 'Enjoy all of our products. (You decide)';
    }
  };

  const getCategoryHeading = (category) => {
    if (category) {
      return category.toUpperCase() + ' PRODUCTS';
    } else {
      return 'ALL PRODUCTS';
    }
  };

  return (
    <Fragment>
      <h2 className="text-center mb-2 mt-3">{getCategoryHeading(selectedCategory)}</h2>
      <p className="text-muted text-center mb-4">{getSubtitleByCategory(selectedCategory)}</p>

      <Container>
        <Row>
          <Col xs={12} sm={4} md={3} lg={2} className="mb-3">
            <div className="d-flex flex-column flex-wrap">
              <div className="d-flex align-items-center mb-3">
                <img src="https://cdn-icons-png.flaticon.com/512/1037/1037762.png" alt="All" style={{ width: '24px', height: '24px', marginRight: '8px' }} />
                <span onClick={() => handleCategoryFilter(null)}>All</span>
              </div>
              <div className="d-flex align-items-center mb-3">
                <img src="https://cdn-icons-png.flaticon.com/512/10905/10905510.png" alt="Snacks" style={{ width: '24px', height: '24px', marginRight: '8px' }} />
                <span onClick={() => handleCategoryFilter('Snacks')}>Snacks</span>
              </div>
              <div className="d-flex align-items-center mb-3">
                <img src="https://cdn-icons-png.flaticon.com/512/3173/3173469.png" alt="Sweets" style={{ width: '24px', height: '24px', marginRight: '8px' }} />
                <span onClick={() => handleCategoryFilter('Sweets')}>Sweets</span>
              </div>
              <div className="d-flex align-items-center mb-3">
                <img src="https://cdn-icons-png.flaticon.com/512/2881/2881698.png?token=exp=1669845166~hmac=405ad8d383d84d6a40b3a11eab0b0d88" alt="Beverages" style={{ width: '24px', height: '24px', marginRight: '8px' }} />
                <span onClick={() => handleCategoryFilter('Beverages')}>Beverages</span>
              </div>
            </div>
          </Col>
          <Col sm={8} md={9} lg={10}>
            <Row className="bg-light">
              {filteredProducts.map((product) => (
                <Col key={product._id} sm={6} md={4} lg={4} className="mt-3 mb-3">
                  <ProductCard productProp={product} />
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}
